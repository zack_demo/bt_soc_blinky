/*
 * app_radar.c
 *
 *  Created on: 4 Jun 2021
 *      Author: zack
 */
#include "app_radar.h"
#include <string.h>

#include "app_log.h"
#include "app_assert.h"

#include "sl_simple_timer.h"
//#include "app_vendor_model.h"

sl_iostream_t *pradar_uart_handler = NULL;

#define RADAR_TX_BUF_LEN	20
#define RADAR_RX_BUF_LEN	55

#define RADAR_RX_PERIOD		100
#define RADAR_RX_TIMEOUT	200
sl_simple_timer_t radar_cmd_timeout_timer;
sl_simple_timer_t radar_rx_timer;
sl_simple_timer_t radar_rx_timeout_timer;

// Line buffer
static uint8_t radar_line_buffer[RADAR_RX_BUF_LEN];
static size_t num_data_in_line_buf = 0;

static bool radar_cmd_idle = true;
uint8_t radar_tx_buf[RADAR_TX_BUF_LEN];

static radar_properties_t radar_properties = {0};

radar_event_IN_cb app_radar_event_IN_cb = NULL;
radar_event_OUT_cb app_radar_event_OUT_cb = NULL;

void (*app_radar_set_cb)(uint8_t cmd, uint8_t result) = NULL;
void (*app_radar_get_cb)(uint8_t cmd, data_union data) = NULL;

void app_radar_send_cmd(radar_cmd_e command, void *payload, uint16_t payload_len);

void app_radar_cmd_timeout_cb();
void app_radar_receive_data_cb();
void app_radar_clear_line_buf();

char* app_radar_get_cmd_str(radar_cmd_e cmd);

void app_radar_register_event_IN_cb(radar_event_IN_cb cb){
	app_radar_event_IN_cb = cb;
}
void app_radar_register_event_OUT_cb(radar_event_OUT_cb cb){
	app_radar_event_OUT_cb = cb;
}

uint16_t crc16(uint8_t *src_data, uint32_t src_data_len) {
	uint16_t crc = 0xFFFF;
	for (uint8_t i = 0; i < src_data_len; i++) {
		crc = ((uint8_t)(crc >> 8) | (crc << 8)) ^ src_data[i];
		crc ^= (uint8_t)(crc & 0xFF) >> 4;
		crc ^= (crc << 12);
		crc ^= ((crc & 0xFF) << 5);
	}
	return (crc);
}

radar_cmd_e init_cmd_list[]= {
		maximum_detection_range,
		detection_sensitivity,
		detection_output,
		minimum_detection_range,
		macro_threshold,
		micro_threshold,
		macro_valid,
		micro_valid,
		detection_mode,
		macro_detection_trigger_range,
		macro_detection_trigger_delay
};

static uint8_t init_cmd_idx = 0, init_cmd_len = sizeof(init_cmd_list)/sizeof(radar_cmd_e);
void app_radar_init_rx_cb(uint8_t cmd, data_union data){
	(void)data;
	if (init_cmd_list[init_cmd_idx] == cmd) init_cmd_idx++;
}

SL_WEAK void app_radar_init_done_cb(bool success){
	(void)success;
}

float macro_valid_timeout = 1.0f;
float micro_valid_timeout = 5.0f;

void app_radar_init_state_machine(){
	static void *temp_get_cb;
	static sl_simple_timer_t state_machine_timer;

	if (init_cmd_idx == 0){
		temp_get_cb = app_radar_get_cb;
	}

	if (init_cmd_idx < init_cmd_len){
		if (radar_cmd_idle == true) {
			app_radar_get_cb = app_radar_init_rx_cb;
			app_radar_send_cmd(init_cmd_list[init_cmd_idx], NULL, 0);
		}
	}

	else if (radar_properties.macro_valid != macro_valid_timeout) {
		if (radar_cmd_idle == true) {
			app_radar_get_cb = NULL;
			app_radar_send_cmd(macro_valid, &macro_valid_timeout, 4);
		}
	}
	else if (radar_properties.micro_valid != micro_valid_timeout) {
		if (radar_cmd_idle == true) {
			app_radar_get_cb = NULL;
			app_radar_send_cmd(micro_valid, &micro_valid_timeout, 4);
		}
	}
	else{
		app_radar_get_cb = temp_get_cb;
		sl_simple_timer_stop(&state_machine_timer);
		app_radar_init_done_cb(true);
		return;
	}
	sl_simple_timer_start(
			&state_machine_timer,
			200,
			app_radar_init_state_machine,
			(void *)NULL,
			false
	);
}

void app_radar_init(sl_iostream_t *pradar_uart_handlerHandler){
	pradar_uart_handler = pradar_uart_handlerHandler;
	sl_status_t sc = sl_simple_timer_start(
			&radar_rx_timer,
			RADAR_RX_PERIOD,
			app_radar_receive_data_cb,
			(void *)NULL,
			false
	);
	app_assert_status_f(sc, "Failed to start radar RX periodic timer\r\n");
	app_radar_clear_line_buf();

	app_radar_init_state_machine();
}

void app_radar_send_cmd(radar_cmd_e command, void *payload, uint16_t payload_len){
	app_log_debug("Radar %s %s\r\n", payload_len?"setting":"getting", app_radar_get_cmd_str(command));

	if (pradar_uart_handler == NULL){
		app_log_warning("Radar Uart is not initialized\r\n");
		return;
	}
	if (payload_len > (RADAR_RX_BUF_LEN - 6)){
		app_log_warning("Radar payload length limit exceed\r\n");
		return;
	}

	if (!radar_cmd_idle){
		app_log_warning("Radar command busy\r\n");
	}

	uint8_t *pbuf = radar_tx_buf;

	// SOF, CMD
	*pbuf++ = 0xd9;
	*pbuf++ = (uint8_t)command;

	// Payload length
	memcpy(pbuf, &payload_len, 2);
	pbuf += 2;

	// Payload data
	if (payload_len > 0){
		memcpy(pbuf, payload, payload_len);
		pbuf += payload_len;
	}

	// CRC
	uint16_t crc = crc16(radar_tx_buf, pbuf - radar_tx_buf);
	memcpy(pbuf, &crc, 2);
	pbuf += 2;

	app_log_debug("Radar Send Command: ");
	for (uint8_t *p = radar_tx_buf; p < pbuf; p++){
		app_log_debug("%02X ", *p);
	}
	app_log_debug("\r\n");

	radar_cmd_idle = false;
	sl_iostream_write(pradar_uart_handler, radar_tx_buf, payload_len + 6);

	sl_simple_timer_start(
			&radar_cmd_timeout_timer,
			1000,
			app_radar_cmd_timeout_cb,
			(void *)NULL,
			false
	);
}

void app_radar_cmd_timeout_cb(){
	app_log_warning("Radar command timeout\r\n");
	app_radar_clear_line_buf();;
}

void app_radar_clear_line_buf(){
	memset(radar_line_buffer, '\0', RADAR_RX_BUF_LEN);
	num_data_in_line_buf = 0;
	radar_cmd_idle = true;
}

void app_radar_parse_payload(radar_cmd_e cmd, uint8_t *payload, data_union *data){
	switch (cmd){
	case maximum_detection_range:
	case minimum_detection_range:
	case macro_threshold:
	case micro_threshold:
	case macro_valid:
	case micro_valid:
		data->f_data = *(float*)payload;
		break;

	case detection_sensitivity:
	case detection_output:
	case detection_enable:
	case detection_mode:
	case macro_detection_trigger_range:
	case macro_detection_trigger_delay:
	case calibration_mode:
	case calibration_message:
	case calibration_message_output_rage:
		data->u_data = *(uint8_t*)payload;
		break;

	default: break;
	}
}

void app_radar_update_properties(radar_cmd_e cmd, data_union data){
	app_log_debug("Radar update property of %s\r\n", app_radar_get_cmd_str(cmd));

	switch (cmd){
	case maximum_detection_range:
		radar_properties.maximum_detection_range = data.f_data;
		break;

	case detection_sensitivity:
		radar_properties.detection_sensitivity = data.u_data;
		break;

	case detection_output:
		radar_properties.detection_output = data.u_data;
		break;

	case detection_enable:
		radar_properties.detection_enable = data.u_data;
		break;

	case minimum_detection_range:
		radar_properties.minimum_detection_range = data.f_data;
		break;

	case macro_threshold:
		radar_properties.macro_threshold = data.f_data;
		break;

	case micro_threshold:
		radar_properties.micro_threshold = data.f_data;
		break;

	case macro_valid:
		radar_properties.macro_valid = data.f_data;
		break;

	case micro_valid:
		radar_properties.micro_valid = data.f_data;
		break;

	case detection_mode:
		radar_properties.detection_mode = data.u_data;
		break;

	case macro_detection_trigger_range:
		radar_properties.macro_detection_trigger_range = data.u_data;
		break;

	case macro_detection_trigger_delay:
		radar_properties.macro_detection_trigger_delay = data.u_data;
		break;

	default:
		app_log_warning("Unknown property command %02X\r\n", cmd);
		break;
	}
}

void app_radar_handle_line_buffer(){
	radar_cmd_e cmd = radar_line_buffer[1];
	uint8_t *payload = radar_line_buffer + 4;

	//	app_log("Handle line buffer\r\n");
	data_union data;

	switch (cmd){
	case ack:
	{
		radar_cmd_e ack_cmd = (radar_cmd_e)(*payload);
		uint8_t ack_result = *(payload + 1);
		switch (ack_result){
		case 1:		// Success
		{
			app_log_debug("Radar command success (%02X)\r\n", ack_cmd);

			radar_cmd_e txcmd = (radar_cmd_e)(*(radar_tx_buf + 1));
			uint8_t *txdata = radar_tx_buf + 4;

			app_radar_parse_payload(txcmd, txdata, &data);
			app_radar_update_properties(txcmd, data);
			break;
		}
		case 0:		// Fail
			app_log_warning("Radar command failed (%02X)\r\n", ack_cmd);
			break;
		case 0xff:	// Unsupported command
			app_log_warning("Radar unsupported command (%02X)\r\n", ack_cmd);
			break;
		default:
			app_log_warning("Radar Unknown ACK response\r\n");
			break;
		}

		// ACK callback
		if (app_radar_set_cb) app_radar_set_cb(ack_cmd, ack_result);
		break;
	}
	case detection_in_event:
	{
		uint64_t event_time = *(uint64_t*)(payload);
		float distance = *(float*)(payload + 8);
		float accuracy = *(float*)(payload + 12);

		app_log_info("Radar IN event: timestamp %dms, distance %.2Fm, accuracy %.2Fm\r\n", event_time, distance, accuracy);

		// Event IN callback
		if (app_radar_event_IN_cb != NULL){
			app_radar_event_IN_cb(event_time, distance, accuracy);
		}
		break;
	}
	case detection_out_event:
	{
		uint64_t event_time = *(uint64_t*)(payload);

		app_log_info("Radar OUT event: timestamp %dms\r\n", event_time);

		// Event OUT callback
		if (app_radar_event_IN_cb != NULL){
			app_radar_event_OUT_cb(event_time);
		}
		break;
	}
	default:
		app_radar_parse_payload(cmd, payload, &data);
		app_radar_update_properties(cmd, data);

		// Get callback
		if (app_radar_get_cb)app_radar_get_cb(cmd, data);
		break;
	}
}

void app_radar_rx_timeout_cb(){
	app_radar_clear_line_buf();
	app_log_debug("Rx timeout\r\n");
}

// return size_t: Size of buffer that has been processed.
size_t app_radar_parse_buffer(uint8_t *pBuf, size_t buf_len){
	size_t processed_len = 0;

	// Determine what to copy
	uint8_t *pSrcStart, *pDestStart;

	if (num_data_in_line_buf == 0){
		// Find start of line position
		uint8_t *pSOL = memchr(pBuf, 0xd9, buf_len);
		if (pSOL == NULL) return buf_len;

		pSrcStart = pSOL;
		pDestStart = radar_line_buffer;
		processed_len = pSOL - pBuf;
	}
	else {
		pSrcStart = pBuf;
		pDestStart = radar_line_buffer + num_data_in_line_buf;
	}

	// Determine copy length
	size_t copyLen;
	size_t expected_payload_len = 0;

	if (num_data_in_line_buf < 4){
		// Get 4 bytes to get payload length first
		copyLen = 4 - num_data_in_line_buf;

		sl_simple_timer_stop(&radar_rx_timeout_timer);
		sl_simple_timer_start(
				&radar_rx_timeout_timer,
				RADAR_RX_TIMEOUT,
				app_radar_rx_timeout_cb,
				NULL,
				false
				);
	}
	else {
		// Payload + 2 byte CRC
		expected_payload_len = *(uint16_t*)&radar_line_buffer[2] + 2;
		if (expected_payload_len > 50){		// Check for non-sense length
			// Cleanup
			app_radar_clear_line_buf();
			sl_simple_timer_stop(&radar_cmd_timeout_timer);
			return buf_len;
		}
		uint16_t payload_len_in_buf = num_data_in_line_buf - 4;
		uint16_t remain_payload_len = expected_payload_len - payload_len_in_buf;

		copyLen = (buf_len < remain_payload_len) ? buf_len : remain_payload_len;

		app_log_debug("Expected len: %d, Copy len: %d, remain length: %d\r\n",
				expected_payload_len,
				copyLen,
				remain_payload_len);
	}

	if (copyLen == 0) return processed_len; // Nothing to copy


	memcpy(pDestStart, pSrcStart, copyLen);
	num_data_in_line_buf += copyLen;
	processed_len += copyLen;

	// Update expected_payload_len
	if (num_data_in_line_buf == 4){
		// Payload + 2 byte CRC
		expected_payload_len = *(uint16_t*)&radar_line_buffer[2] + 2;
	}

	// Process line buffer if ready
	if (num_data_in_line_buf == expected_payload_len + 4){
		sl_simple_timer_stop(&radar_rx_timeout_timer);

		uint16_t crc = *(uint16_t *)&radar_line_buffer[num_data_in_line_buf - 2];
		uint16_t calcrc = crc16(radar_line_buffer, num_data_in_line_buf - 2);

		app_log_debug("Radar RX: ");
		for (size_t i = 0; i < num_data_in_line_buf; i++){
			app_log_debug("%02X ", radar_line_buffer[i]);
		}
		app_log_debug("\r\n");

		if (crc == calcrc){
			app_radar_handle_line_buffer();
		}
		else{
			app_log_warning("Radar RX buffer CRC error: expected %04X, calculated %04X\r\n", crc, calcrc);
		}

		// Cleanup
		app_radar_clear_line_buf();
		sl_simple_timer_stop(&radar_cmd_timeout_timer);
	}

	//	app_log("Processed %d bytes\r\n", processed_len);
	return processed_len;
}

void app_radar_receive_data_cb(){
	sl_status_t sc;
	uint8_t buffer[RADAR_RX_BUF_LEN];
	size_t num_buffer;

	do {
		sc = sl_iostream_read(pradar_uart_handler,
				buffer,
				RADAR_RX_BUF_LEN,
				&num_buffer);
		//	app_assert_status_f(sc, "Failed to read lora UART buffer\r\n");

		// Exit the loop if nothing to read
		if (num_buffer == 0) {
			break;
		}

		//		app_log("Handle rx buffer\r\n");
		uint8_t* pbuf = buffer;
		while (num_buffer > 0){
			size_t len = app_radar_parse_buffer(pbuf, num_buffer);

			app_log_debug("Processed %d bytes\r\n", len);

			num_buffer -= len;
			pbuf += len;
		}
	} while (1);

	sc = sl_simple_timer_start(
			&radar_rx_timer,
			RADAR_RX_PERIOD,
			app_radar_receive_data_cb,
			(void *)NULL,
			false
	);
	app_assert_status_f(sc, "Failed to start radar RX periodic timer\r\n");
}

char* app_radar_get_cmd_str(radar_cmd_e cmd){
	switch (cmd){
	case version: return "Version";
	case ack: return "ACK";
	case maximum_detection_range: return "maximum_detection_range";
	case detection_sensitivity: return "detection_sensitivity";
	case detection_output: return "detection_output";
	case detection_in_event: return "detection_in_event";
	case detection_out_event: return "detection_out_event";
	case reset_configuration: return "reset_configuration";
	case get_detection_status: return "get_detection_status";
	case detection_enable: return "detection_enable";
	case minimum_detection_range: return "minimum_detection_range";
	case macro_threshold: return "macro_threshold";
	case micro_threshold: return "micro_threshold";
	case macro_valid: return "macro_valid";
	case micro_valid: return "micro_valid";
	case detection_mode: return "detection_mode";
	case macro_detection_trigger_range: return "macro_detection_trigger_range";
	case macro_detection_trigger_delay: return "macro_detection_trigger_delay";
	default: return "Unknown";
	}
}
