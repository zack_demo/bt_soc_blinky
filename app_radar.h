/*
 * app_radar.h
 *
 *  Created on: 4 Jun 2021
 *      Author: zack
 */

#ifndef APP_RADAR_H_
#define APP_RADAR_H_

#include "sl_iostream.h"
#include <stdbool.h>

typedef enum {
	version = 0x00,
	ack = 0x02,
	maximum_detection_range = 0x03,
	detection_sensitivity = 0x04,
	detection_output = 0x05,
	detection_in_event = 0x06,
	detection_out_event = 0x07,
	reset_configuration = 0x08,
	get_detection_status = 0x09,
	detection_enable = 0x0a,
	calibration_mode = 0x0f,
	calibration_message = 0x10,
	calibration_message_output_rage = 0x11,
	minimum_detection_range = 0x30,
	macro_threshold = 0x31,
	micro_threshold = 0x32,
	macro_valid = 0x33,
	micro_valid = 0x34,
	detection_mode = 0x35,
	macro_detection_trigger_range = 0x36,
	macro_detection_trigger_delay = 0x37,
} radar_cmd_e;

typedef enum {
	low = 0,
	meidum,
	high,
	custom
} detection_sensitivity_e;

typedef enum {
	macro_then_micro = 0,
	macro_only,
	micro_only,
	macro_and_micro
} detect_mode_e;

typedef struct radar_properties_s {
	float maximum_detection_range;
	detection_sensitivity_e detection_sensitivity;
	bool detection_output;
	bool detection_enable;
	float minimum_detection_range;
	float macro_threshold;
	float micro_threshold;
	float macro_valid;
	float micro_valid;
	detect_mode_e detection_mode;
	uint8_t macro_detection_trigger_range;
	uint8_t macro_detection_trigger_delay;
}radar_properties_t;

typedef union {
	float f_data;
	uint8_t u_data;
	bool b_data;
} data_union;

typedef void (*radar_event_IN_cb)(uint64_t event_time, float distance, float accuracy);
typedef void (*radar_event_OUT_cb)(uint64_t event_time);

void app_radar_init(sl_iostream_t *pUartHandler);
void app_radar_init_done_cb(bool success);
void app_radar_send_cmd(radar_cmd_e command, void *payload, uint16_t payload_len);
void app_radar_register_event_IN_cb(radar_event_IN_cb cb);
void app_radar_register_event_OUT_cb(radar_event_OUT_cb cb);

#endif /* APP_RADAR_H_ */
